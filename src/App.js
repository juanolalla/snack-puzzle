import { useState, useEffect } from "react";
import Score from "./components/Score";

import chaskis from './img/chaskis.jpg'
import cheesesticks from './img/cheesesticks.jpg'
import pandilla from './img/pandilla.jpg'
import pelotazos from './img/pelotazos.jpg'
import trigosnack from './img/trigosnack.jpg'
import yorkeso from './img/yorkeso.jpg'
import blank from './img/blank.png'


const snackElements = [
  chaskis,
  cheesesticks,
  pandilla,
  pelotazos,
  trigosnack,
  yorkeso,
];

const width = 8;
//const snackElements = ["red", "yellow", "green", "purple", "blue", "cyan"];

const App = () => {
  const [currentElementsList, setCurrentElementList] = useState([]);
  const [pieceDragged, setPieceDragged] = useState(null)
  const [pieceReplaced, setPieceReplaced] = useState(null)
  const [scoreBoard, setScoreBoard] = useState(0)

  const checkVerticalMatchesThree = () => {
    for (let x = 0; x < width * width - 2 * width; x++) {
      const verticalMatch = [x, x + width, x + width * 2];
      const elementMatched = currentElementsList[x];
      const isBlank = currentElementsList[x] === blank

      if (
        verticalMatch.every(
          (square) => currentElementsList[square] === elementMatched && !isBlank
                 ) )
       {
        verticalMatch.forEach(square => currentElementsList[square] = blank);
        setScoreBoard((score)=>score + 3)
       // verticalMatch.forEach(square => currentElementsList[square] = slidePieces(currentElementsList[square]))
       return true
      }
    }
  };



  const checkVerticalMatchesFour = () => {
    for (let x = 0; x < (width * width)- 3 * width; x++) {
      const verticalMatch = [x, x + width, x + width * 2,  x + width * 3];
      const elementMatched = currentElementsList[x];
      const isBlank = currentElementsList[x] === blank

      if (
        verticalMatch.every(
          (square) => currentElementsList[square] === elementMatched && !isBlank
       ) )
       {
        verticalMatch.forEach(square => currentElementsList[square] = blank);
        setScoreBoard((score)=>score + 4)
        return true
        //verticalMatch.forEach(square => currentElementsList[square] = slidePieces(currentElementsList[square]))
      }
    }
  };

  const checkHorizontalMatchesThree = () => {
    for(let x = 0; x < width * width; x++){
 //   for (let y = 0; y < width-2; y++) {
      
      const horizontalMatch = [x, x + 1, x + 2];
      const elementMatched = currentElementsList[x];
      const isBlank = currentElementsList[x] === blank

      if (
        horizontalMatch.every(
          (square) => currentElementsList[square] === elementMatched && !isBlank
       ) )
       {
        horizontalMatch.forEach(square => currentElementsList[square] = blank);
        setScoreBoard((score)=>score + 3)

        return true
        //horizontalMatch.forEach(square => currentElementsList[square] = slidePieces(currentElementsList[square]))
   //   }
    }}
  };

  const checkHorizontalMatchesFour = () => {
    for (let x = 0; x < width*width; x++) {
      //for(let y = 0; y < width - 2; y++) {
      const horizontalMatch = [x, x + 1, x + 2,  x + 3];
      const elementMatched = currentElementsList[x];
      const isBlank = currentElementsList[x] === blank

      if (
        horizontalMatch.every(
          (square) => currentElementsList[square] === elementMatched && !isBlank
       ) )
       {
        horizontalMatch.forEach(square => currentElementsList[square] = blank);
        //horizontalMatch.forEach(square => currentElementsList[square] = slidePieces(currentElementsList[square]))
        setScoreBoard((score)=>score + 4)
        return true
      //}
    }}
  };



 const slidePieces = () =>{
   for (let x = 0; x < width * width; x++) {
     const firstRow = [0,1,2,3,4,5,6,7]
     const isFirstRow = firstRow.includes(x)

     if(isFirstRow && currentElementsList[x] === blank){
       currentElementsList[x] = snackElements[Math.floor(Math.random() * snackElements.length)]
     }

     if((currentElementsList[x + width]) === blank){
       currentElementsList[x + width] = currentElementsList[x]
       currentElementsList[x] = blank
     }
     
   }
 }


const dragStart = (e) =>{
  setPieceDragged(e.target)
}



const dragDrop = (e) =>{
  setPieceReplaced(e.target)
}


const dragEnd = (e) =>{
  const pieceDraggedId =  parseInt(pieceDragged.getAttribute('data-id'))
  const pieceReplacedId = parseInt(pieceReplaced.getAttribute('data-id'))

  currentElementsList[pieceReplacedId] = pieceDragged.getAttribute('src')
  currentElementsList[pieceDraggedId] = pieceReplaced.getAttribute('src')



const validMoves = [
  pieceDraggedId -1,
  pieceDraggedId - width,
  pieceDraggedId + 1,
  pieceDraggedId + width
]

const validMove = validMoves.includes(pieceReplacedId)

const isAHorizontalMatchOfFour = checkHorizontalMatchesFour()
const isAHorizontalMatchOfThree = checkHorizontalMatchesThree()
const isAVerticalMatchOfFour = checkVerticalMatchesFour()
const isAVerticalMatchOfThree = checkVerticalMatchesThree()

if(pieceReplacedId && validMove && (isAHorizontalMatchOfThree || isAVerticalMatchOfFour || isAHorizontalMatchOfFour || isAVerticalMatchOfThree)){
  setPieceDragged(null)
  setPieceReplaced(null)
}

else{
  currentElementsList[pieceReplacedId] = pieceReplaced.getAttribute('src')
  currentElementsList[pieceDraggedId] = pieceDragged.getAttribute('src')
  setCurrentElementList([...currentElementsList])
}

}

  const createBoard = () => {
    const randomElements = [];
    for (let x = 0; x < width * width; x++) {
      const randomElement =
        snackElements[Math.floor(Math.random() * snackElements.length)];
      randomElements.push(randomElement);
    }
    setCurrentElementList(randomElements);
  };

  useEffect(() => {
    createBoard();
  }, []);

  useEffect(() => {
    /**
     * Establishing a timer in order to check the vertical matches each 100 ms
     */
    const timer = setInterval(() => {
      checkHorizontalMatchesFour()
      checkHorizontalMatchesThree()
      
      checkVerticalMatchesFour()
      checkVerticalMatchesThree();
      slidePieces()
      setCurrentElementList([...currentElementsList])

      
    }, 100);
    return () => clearInterval(timer);
  }, [checkHorizontalMatchesFour, checkHorizontalMatchesThree, checkVerticalMatchesFour,checkVerticalMatchesThree,slidePieces, currentElementsList]);

  console.log(currentElementsList);

  return (
    <div className="app">
      <div className="game">
        {currentElementsList.map((element, index) => {
          return (
            <img
              key={index}
              src={element}
              style={{ backgroundColor: element }}
              alt={element}
              data-id = {index}
              draggable = {true}
              onDragStart = {dragStart}
              onDragOver = {(e) => e.preventDefault() }
              onDragEnter = {(e) => e.preventDefault() }
              onDragLeave = {(e) => e.preventDefault() }
              onDrop = {dragDrop}
              onDragEnd = {dragEnd}
            ></img>
          );
        })}
      </div>
      <Score score={scoreBoard}>SNACKS????</Score>
    </div>
  );
};

export default App;
